package TestNgScript;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Screept {
	@BeforeClass
	public void beforeClass() {
		System.out.println("BeforeClass method of TestNgscreept Class");
	}

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("BeforeMethod of TestNgscreept class");
	}

	@Test(enabled=false)
	public void test() {
		System.out.println("TestMethod of TestNG class");
	}

	@Test(priority=-1)
	public void test1() {
		System.out.println("Test1 method of TesNG Class");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("After Method of TestNg Class");
	}

	@AfterClass
	public void afterClass() {
		System.out.println("AfterClass method of Testscreept class");
	}
}

	
	


