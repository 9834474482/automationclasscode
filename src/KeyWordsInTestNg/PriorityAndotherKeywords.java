package KeyWordsInTestNg;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class PriorityAndotherKeywords {
	//	public class PriorityKeywordTestNG {
			WebDriver driver;
		@BeforeTest
		@Parameters("Browsers")
		public void beforeTest(String browserName) {
			System.out.println("Execute Before Class exec Start");
			System.out.println(browserName);
			if(browserName.equalsIgnoreCase("Chrome"));{
				System.setProperty("webdriver.chrome.driver", "C:\\chrome\\chromedriver_win32\\chromedriver.exe");
				driver=new ChromeDriver();
				driver.manage().window().maximize();
			}

		}
		@BeforeClass
		public void beforeClass(){
			System.out.println("BeforeClass Method");
		}
		@BeforeMethod
		public void beforMethod() {
			System.out.println("BeforeMethod used");
		}//Keyword of TestNG
		//@Test(priority=2)
		@Test(invocationCount=2)
		//@Test(dependsOnMethods="test1")
		//@Test(enabled=false)
		//@Test(timeOut=3000)
		public void test() throws InterruptedException {
			//Thread.sleep(4000);
			System.out.println("Test method used");
		}
		@Test
		public void test1() {
		System.out.println("Test1 method used");
		}
		@AfterMethod
		public void afterMethod() {
			System.out.println("After method used");
			
		}
		@AfterClass
		public void afterClass(){
			System.out.println("After Class method");
		}
		@AfterTest
		public void afterTest() {
			System.out.println("Execute After Class exec complt");

		}
		}

//}
