package Grouping;

import org.testng.Reporter;
import org.testng.annotations.Test;

public class Group_ {
@Test(groups="Login")
public void TC1(){
	Reporter.log("Runnig TC1", true);
}
@Test(groups="payment")
public void TC2() {
	Reporter.log("Runnig TC2", true);
}
@Test(groups="Login")
public void TC3() {
	Reporter.log("Runnig TC3", true);
}
@Test
public void TC4() {
	Reporter.log("Runnig TC4", true);
}
}
